class Friendship < ApplicationRecord

  belongs_to :user
  belongs_to :friend, class_name: "User"

  validate :uniquess_of_friendship

  private

  def uniquess_of_friendship
    user.friends.where(id: friend.id).pluck(:name)
    if user && friend && user.friends.where(id: friend.id).any?
      errors.add(:base, "Les utilisateurs sont déjà amis.")
    end
  end

end
