class Message < ApplicationRecord
  TIME_TO_WAIT = 2.hours
  validates :content, presence: true, length: { minimum: 280 }
  belongs_to :user
  belongs_to :recipient, class_name: "User"

end
