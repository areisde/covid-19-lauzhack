class User < ApplicationRecord

  has_secure_password

  validates :firstname, presence: true, length: { maximum: 50 }
  validates :lastname, presence: true, length: { maximum: 50 }
  validates :email, format: { with: /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/ }, uniqueness: { case_sensitive: false }

  has_many :messages

  has_many :friendships, dependent: :destroy
  has_many :inverse_of_friendships, foreign_key: "friend_id", class_name: "Friendship", dependent: :destroy

  before_save :create_remember_token
  before_create :create_uuid

  def friends
    unless @friends
      invited = User.joins(:friendships).where("friendships.friend_id = ? AND friendships.accepted = ?", id, true).pluck(:id)
      received = User.joins(:inverse_of_friendships).where("friendships.user_id = ? AND friendships.accepted = ?", id, true).pluck(:id)
      @friends = User.where("id IN (?)", invited + received - [self.id])
    end
    @friends
  end

  # friend request sent but not acknowledged yet
  def pending_requests
    @pending_requests ||= self.friendships.where(pending: true)
  end

  # incoming request to be friend to be acknowledged
  def incoming_requests
    @incoming_requests ||= self.inverse_of_friendships.where(pending: true)
  end

  def full_name
    "#{firstname} #{lastname}"
  end

  private

  def create_remember_token
    self.remember_token = SecureRandom.urlsafe_base64
  end

  def create_uuid
    self.uuid = SecureRandom.hex(10)
  end

end
