class ApplicationController < ActionController::Base
  include SessionsHelper

  add_flash_types :success, :error

  protect_from_forgery

  def redirect_if_connected
    redirect_to root_path, error: "Vous êtes déjà connecté !" if signed_in?
  end

  def redirect_if_not_connected
    redirect_to root_path, success: "Veuillez vous connecter." unless signed_in?
  end


end
