class MessagesController < ApplicationController
  before_action :redirect_if_not_connected, :fetch_recipient
  layout "mailbox"

  def new     # Get request
    @message = Message.new
    @message.user = current_user()
    @message.recipient = @recipient
  end
  
  def show    # Get request
    # Display a specific message to the current user
    @message = Message.find(params[:id])
    if @message.created_at + Message::TIME_TO_WAIT < Time.now
      redirect_to messages_path(uuid: @recipient.uuid), error: "Vous devez attendre que le message arrive"
    end
  end 
  
  def create  # post request
    @message = Message.new(message_params)
    @message.user = current_user()
    @message.recipient = @recipient # @recipient defined with before_action as member variable 
    if @message.save
      redirect_to messages_path(uuid: @recipient.uuid), success: "Votre message a bien été envoyé"
    else
      render 'new'
    end
  end

  def index   # get request
    # Display all Messages (sent & received) from specific friend 
    @sent_messages  = current_user.messages.where(recipient: @recipient)
    @received_messages = @recipient.messages.where(recipient: current_user)
  end

  private
  def message_params()
    params.require(:message).permit(
      :content
    )
  end

  def fetch_recipient()
    @recipient = current_user.friends.find_by(uuid: params[:uuid])
    if  !@recipient 
      redirect_to root_path, error: "Votre destinataire n'existe pas"
    end
  end
end
