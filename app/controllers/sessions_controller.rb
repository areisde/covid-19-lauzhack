class SessionsController < ApplicationController
  before_action :redirect_if_connected, only: [:new, :create]
  before_action :redirect_if_not_connected, only: [:destroy]

  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].strip.downcase)
    if @user && @user.authenticate(params[:session][:password])
      sign_in @user, permanent: params[:session][:remember_me] == "1"
      redirect_to root_path, success: "Vous êtes connecté !"
    else
      flash.now[:error] = "Nom d'utilisateur et/ou mot de passe incorrect(s)"
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_path, success: "Vous êtes déconnecté !"
  end

end
