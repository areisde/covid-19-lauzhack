class PagesController < ApplicationController
  before_action :redirect_if_not_connected, only: :dashboard

  def home
    @user = User.new
  end

  def dashboard
    render layout: "mailbox"
  end

end

