class FriendshipsController < ApplicationController
  before_action :redirect_if_not_connected

  # create a friend request
  def create
    @friend = User.find_by(uuid: params[:uuid])
    @friendship = Friendship.new(user: current_user, friend: @friend, pending: true)
    if @friendship.save
      redirect_to root_path, success: "Votre demande a été envoyée !"
    else
      redirect_to root_path, error: "Il y a eu un problème avec votre demande."
    end
  end

  def create_random_friendship
    tab_id = User.ids # I hope it works
    rd = rand(tab_id.length)
    @friend = User.find_by(id: rd)
    @friendship = Friendship.new(user: current_user, friend: @friend, pending: true)
    if @friendship.save
      redirect_to dashboard_path, success: "Votre demande a été envoyée !"
    else
      redirect_to dashboard_path, error: "Il y a eu un problème avec votre demande."
    end
  end

  # accept a friend request
  def update
    @friendship = current_user.incoming_requests.find(params[:id])
    @friendship.update(pending: false, accepted: true)
    redirect_to root_path, success: "Vous pouvez maintenant écrire à #{@friendship.friend.full_name}"
  end

  # refuse a friend request
  def destroy
    @friendship = current_user.incoming_requests.find(params[:id])
    @friendship.update(pending: false, accepted: false)
    redirect_to root_path, success: "Vous avez refusé l'invitation de #{@friendship.friend.full_name}"
  end

end
