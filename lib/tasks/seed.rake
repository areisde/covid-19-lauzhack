namespace :seed do

  desc "Setup an example app"
  task example: :environment do
    firstnames = %w(Albert Michel Alexandre Martin)
    lastnames = %w(Dupont Borcard Poulin Salin)
    users = (0..3).map do |i|
      User.create!(firstname: firstnames[i], lastname: lastnames[i], 
        email: "#{firstnames[i].downcase}@#{lastnames[i].downcase}.com", 
        password: "12341", password_confirmation: "12341")
    end
    Friendship.create!(user: users[0], friend: users[1], pending: false, accepted: true)
    Friendship.create!(user: users[0], friend: users[2], pending: true, accepted: nil)
    Friendship.create!(user: users[3], friend: users[0], pending: true, accepted: nil)

    puts "Connect with #{users[0].email} and password 12341"
  end

end