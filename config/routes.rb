Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: "pages#home"

  get "/dashboard/random_friend", to: "friendships#create_random_friendship"
  get "/dashboard", to: "pages#dashboard"

  resources :sessions, only: [:create]
  delete "logout", to: "sessions#destroy"
  get "login", to: "sessions#new"

  scope "/:uuid" do
    resources :messages, only: [:index, :show, :create, :new]
  end

  resources :users, only: [:new, :create]

  resources :friendships, only: [:create, :update, :destroy]

end
