class AddPendingToFriendships < ActiveRecord::Migration[6.0]
  
  def change
    add_column :friendships, :pending, :boolean
    add_column :friendships, :accepted, :boolean
    add_column :friendships, :accepted_at, :datetime
  end

end
