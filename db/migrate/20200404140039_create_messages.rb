class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|

      t.text :content
      t.references :user, null: false, foreign_key: true
      t.bigint :recipient_id

      t.timestamps

    end
    add_foreign_key :messages, :users, column: :recipient_id

  end
end
