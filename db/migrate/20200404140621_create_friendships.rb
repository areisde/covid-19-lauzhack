class CreateFriendships < ActiveRecord::Migration[6.0]
  def change
    create_table :friendships do |t|
      t.references :user, null: false, foreign_key: true
      t.bigint :friend_id

      t.timestamps
    end
    add_foreign_key :friendships, :users, column: :friend_id
  end
end
