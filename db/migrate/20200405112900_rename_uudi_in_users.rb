class RenameUudiInUsers < ActiveRecord::Migration[6.0]
  def change
    rename_column :users, :uudi, :uuid
    change_column :users, :uuid, :string
  end
end
